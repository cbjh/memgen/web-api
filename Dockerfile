ARG MEMGEN_BRANCH="main"
FROM registry.gitlab.com/cbjh/memgen/memgen/22.04:${MEMGEN_BRANCH}

USER root

RUN apt-get install -y nodejs npm
RUN npm install -g n
RUN n 16

ARG MODE="develop"

# Faster build of GROMACS and Amber Tools

RUN \
  if [ "$MODE" = "develop" ]; then \
    apt-get install -y fftw3-dev libarpack2-dev libnetcdf-dev libnetcdff-dev ; \
  fi

USER user
