import path from 'path'
import os from 'os'
import { mkdtemp, rm } from 'fs/promises'
// import shell from 'shelljs'

async function tempExec(callback) {
  const directory = await mkdtemp(path.join(os.tmpdir(), 'memgen-'))

  // shell.cd(directory)

  try {
    const result = await callback(directory)

    // shell.ls('-R').forEach(function(file) {
    //   console.log(file)
    // })

    return result
  } finally {
    await rm(directory, { recursive: true, force: true })
  }
}

export default tempExec
