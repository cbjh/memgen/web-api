import { execaCommand } from 'execa'
import { readFile, writeFile } from 'fs/promises'
import _ from 'lodash'
import pRetry from 'p-retry'

import config from './../config.js'

function memgen({ pdbs, ratio, areaPerLipid, waterPerLipid, lipidsPerMonolayer, addedSalt, boxShape, forceField }) {
  return async function(directory) {
    let i = 1
    let pdbPaths = []

    for (const pdb of pdbs) {
      if (pdb.toString('ascii').substr(0, 5) === 'db://') {
        pdbPaths.push(pdb.toString('ascii'))

        continue
      }

      const pdbPath = `${directory}/input${i}.pdb`

      await writeFile(pdbPath, pdb)

      pdbPaths.push(pdbPath)

      i += 1
    }

    let ratioParam

    if (ratio === undefined) {
      ratioParam = ""
    } else {
      ratioParam = `--ratio ${ratio.join(' ')}`
    }

    let forceFieldParam

    if (forceField === undefined) {
      forceFieldParam = ""
    } else {
      forceFieldParam = `--force-field ${forceField}`
    }

    let cmd = `memgen`
      cmd += ` "${pdbPaths.join('" "')}"`
      cmd += ` final.pdb`
      cmd += ` --png final.png`
      cmd += ` --topology topol.top`
      cmd += ` ${forceFieldParam}`
      cmd += ` --area-per-lipid ${areaPerLipid}`
      cmd += ` --water-per-lipid ${waterPerLipid}`
      cmd += ` --lipids-per-monolayer ${lipidsPerMonolayer}`
      cmd += ` --added-salt ${addedSalt || 0}`
      cmd += ` --box-shape ${boxShape || 'square'}`
      cmd += ` ${ratioParam}`
      cmd += ` --run-locally`

    console.log(`> executing: ${cmd}`)

    const runMemgen = async () => {
      return execaCommand(cmd, { shell: true, cwd: directory })
    }

    let call

    try {
      call = await pRetry(runMemgen, { retries: 3 })
    } catch (error) {
      console.log(`< failed: ${cmd}`)

      return {
        success: false,
        error: "failed to generate the membrane",
        stdOut: error.stdout,
        stdErr: error.stderr
      }
    }

    console.log(`< succeeded: ${cmd}`)

    return {
      success: true,
      pdb: await readFile(`${directory}/final.pdb`),
      png: await readFile(`${directory}/final.png`),
      topology: await readFile(`${directory}/topol.top`)
    }
  }
}

export default memgen
