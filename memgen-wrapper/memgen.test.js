import assert from 'assert'
import { readFile } from 'fs/promises'
import shell from 'shelljs'

import memgen from './memgen.js'
import tempExec from './tempExec.js'

describe('memgen', function () {
  it('should generate a membrane', async function() {
    this.timeout(2 * 40 * 1000)

    const inputPath = shell.exec("realpath example/chol.pdb", { silent: true }).toString().trimEnd()
    const inputFile = await readFile(inputPath)
    
    const result = await tempExec(memgen({
      pdbs: [inputFile],
      areaPerLipid: 65,
      waterPerLipid: 40,
      lipidsPerMonolayer: 64,
      addedSalt: 0,
      boxShape: 'square'
    }))

    assert(result.success === true, result)
  })
})
