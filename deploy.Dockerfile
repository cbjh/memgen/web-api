ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ADD --chown=user:user ./ /home/user/server/

RUN ( cd /home/user/server && npm ci )

CMD ( cd /home/user/server && npm run serve )
